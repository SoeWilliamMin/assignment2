﻿using NUnit.Framework;
using System;
using BowlingGame;
namespace BowlingGameTest
{
    [TestFixture()]
    public class Test
    {
        Game game = new Game();
        public void manyroll(int roll, int pins)
        {
            for (int i = 0; i < roll; i++)
            {
                game.Roll(pins);

            }
        }
        [Test()]
        public void GutterGame()
        {
            for (int i = 0; i < 20; i++)
            {
                game.Roll(0);
            }
            Assert.That(game.Score, Is.EqualTo(0));
        }
        [Test]
        public void OnePinGame()
        {
            manyroll(20, 1);

            Assert.That(game.Score, Is.EqualTo(20));
        }
        [Test]
        public void OneSpareAtFristFrame()
        {
            game.Roll(9);
            game.Roll(1);
            manyroll(18, 1);
            Assert.That(game.Score, Is.EqualTo(29));
        }
        [Test]
        public void OneStrikeAtFirstFrame()
        {
            game.Roll(10);
            manyroll(18, 1);
            Assert.That(game.Score, Is.EqualTo(30));
        }
        [Test]
        public void PerfectGame()
        {
            manyroll(20, 10);
            Assert.That(game.Score, Is.EqualTo(300));
        }
    }
}
